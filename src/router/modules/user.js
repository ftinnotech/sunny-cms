/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

// const userRouter = {
//   path: '/user',
//   component: Layout,
//   children: [
//     {
//       path: 'index',
//       component: () => import('@/views/user/User'),
//       name: 'User',
//       meta: { title: 'User List', icon: 'user', noCache: true }
//     }
//   ]
// }

export default userRouter
