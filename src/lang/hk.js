export default {
  route: {
    user: '用戶管理',
    sample: '更多例子',
    sample1: '例1',
    sample2: '例2'
  },
  navbar: {
    logOut: '退出登錄'
  },
  login: {
    title: '任務管理系統',
    logIn: '登錄',
    username: '賬號',
    password: '密碼'
  },
  general: {
    ID: 'ID',
    create: '新建',
    edit: '編輯',
    delete: '刪除',
    detail: '詳細資料',
    confirm: '確定',
    cancel: '取消',
    success: '操作成功',
    error: '錯誤',
    actions: '操作',
    search: '搜尋'
  },
  error: {
    general: '網絡錯誤，請稍後再試',
    length: '輸入長度在{min}到{max}之間',
    password: '請輸入密碼',
    passwordNotMatch: '兩次密碼不一致',
    email: '請輸入電郵',
    emailNotValid: '請正確輸入電郵',
    user: {
      username: '請輸入用戶名',
      firstName: '請輸入名',
      lastName: '請輸入姓'
    }
  },
  user: {
    username: '用戶名',
    lastLogin: '最後登入',
    create: '新建用戶',
    edit: '編輯用戶',
    status: {
      status: '狀態',
      A: '正常',
      I: '停用',
      D: '已刪除'
    },
    firstName: '名',
    lastName: '姓',
    fullName: '姓名',
    password: '密碼',
    confirmPassword: '重複密碼',
    email: '電郵',
    role: '用戶類型',
    ip: 'IP地址',
    staffId: '員工ID'
  }
}
