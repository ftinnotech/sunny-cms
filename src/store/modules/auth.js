import { getToken, removeToken } from '@/utils/auth'
import request from '@/utils/request'
import Cookies from 'js-cookie'

const auth = {
  state: {
    token: getToken(),
    role: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
      Cookies.set('token', token)
    },
    SET_ROLE: (state, role) => {
      state.role = role
    }
  },

  actions: {

    LoginByUsername({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        request({
          url: '/Auth/login',
          method: 'post',
          data: { username, password: userInfo.password }
        }).then(response => {
          console.log(response)
          Cookies.set('role', response.role)
          commit('SET_TOKEN', response.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        // logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLE', '')
        removeToken()
        Cookies.remove('role')
        resolve()
        // }).catch(error => {
        //   reject(error)
        // })
      })
    }
  }
}

export default auth
