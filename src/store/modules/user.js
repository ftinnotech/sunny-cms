import request from '@/utils/request'

const user = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    // new 1.
    getUserList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/getUsers',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 2.
    deleteUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/deleteUsers',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 3.
    createUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/createUser',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/user/edit',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },

    searchUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/user/search',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editPassword({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/user/editPassword',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    }
  }
}

export default user
